import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/common';

@Injectable()
export class AppService {
  constructor(private httpService: HttpService) {}
  
  getDeviceTelemetry(deviceId: string): any {
    return this.httpService.get(`http://localhost:3001/deviceTelemetry/${deviceId}`);
  }

  setAnalysisResult(deviceId: string, data) {
    return this.httpService.post(`http://localhost:4283/AnalysisResult/${deviceId}`, data);
  }
}
