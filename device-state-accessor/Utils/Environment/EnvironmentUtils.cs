using System;

namespace Utils.Environment
{
    public static class EnvironmentUtils
    {
        public static string GetEnvironmentVariable(string name)
        {
            return System.Environment.GetEnvironmentVariable(name)
                ?? throw new ArgumentException($"{name} environment variable not set");
        }

        public static string GetEnvironmentVariable(string name, string defaultValue)
        {
            return System.Environment.GetEnvironmentVariable(name) ?? defaultValue;
        }

    }
}