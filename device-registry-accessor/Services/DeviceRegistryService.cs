using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Threading.Tasks;
using System;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace DeviceRegistryAccessor.Services
{
    public class DeviceRegistryService
    {
        private IMongoCollection<BsonDocument> _collection;

        public DeviceRegistryService()
        {
            Console.WriteLine("Creating mongo client");
            var host = Environment.GetEnvironmentVariable(Constants.MongoDbServer) ?? "localhost";
            var port = Environment.GetEnvironmentVariable(Constants.MongoDbPort) ?? "27017";
            var client = new MongoClient($"mongodb://{host}:{port}");
            var dbName = Environment.GetEnvironmentVariable(Constants.DbName) ?? "microservicesIotExercise";
            var database = client.GetDatabase(dbName);
            _collection = database.GetCollection<BsonDocument>("DevicesSchamas");
            Seed();

        }

        private void Seed()
        {
            var filterBuilder = Builders<BsonDocument>.Filter;
            var filter = filterBuilder.Eq<string>("id", "sim-device-00");

            if (!_collection.Find(filter).Any())
            {
                var bson = BsonDocument.Parse(JObject.FromObject(new
                {
                    id = "sim-device-00",
                    telemetry = new { state = "on or off" }
                }).ToString());
                
                _collection.InsertOne(bson);
                Console.WriteLine("Seeded");
            }
        }

        public async Task Register(string schema)
        {
            var bson = BsonDocument.Parse(schema);
            await _collection.InsertOneAsync(bson);
        }

        public async Task<string> GetDeviceSchema(string id)
        {
            var filterBuilder = Builders<BsonDocument>.Filter;
            var filter = filterBuilder.Eq<string>("id", id);

            var bsonSchema = await _collection.FindAsync(filter);
            return bsonSchema.FirstOrDefault().ToJson();
        }
    }


}