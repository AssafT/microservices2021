using StackExchange.Redis;
using System;
using System.Text.Json;

namespace DeviceStateAccessor.Services
{

    public class DeviceStateService
    {
        private const string IdProperty = "id";

        private readonly ConnectionMultiplexer _muxer;
        private readonly IDatabase _connection;

        public DeviceStateService(string redisServerName)
        {
            _muxer = ConnectionMultiplexer.Connect("localhost:6379");
            _connection = _muxer.GetDatabase();
        }

        public bool Set(string jsonState)
        {
            var json = JsonDocument.Parse(jsonState);

            Validate(json);

            return _connection.StringSet(GetId(json), jsonState);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns>null if key doesn't exist.</returns>
        public string Get(string id)
        {
            return _connection.StringGet(id);
        }

        private void Validate(JsonDocument json)
        {
            // TODO: Schema validation; until then:
            GetId(json);
        }

        private static string GetId(JsonDocument json)
        {
            return json.RootElement.GetProperty(IdProperty).GetString();
        }
    }


}