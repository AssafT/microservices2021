using DeviceRegistryAccessor.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace DeviceRegistryAccessor.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DeviceRegistryController : Controller
    {
        private readonly DeviceRegistryService _deviceRegistryService;

        public DeviceRegistryController(DeviceRegistryService deviceRegistryService)
        {
            _deviceRegistryService = deviceRegistryService;
        }

        [HttpPost("")]
        public async Task<IActionResult> Register([FromBody] object body)
        //public async Task<IActionResult> Register([FromBody] JsonElement body)
        {
            //using (var reader = new StreamReader(Request.Body, Encoding.UTF8))
            //{
            var bodyAsString = body.ToString();// await reader.ReadToEndAsync();
            //var bodyAsString = System.Text.Json.JsonSerializer.Serialize(body);// await reader.ReadToEndAsync();

            try
            {
                    var json = JsonDocument.Parse(bodyAsString);

                    await _deviceRegistryService.Register(bodyAsString);
                    return Accepted($"DeviceRegistry/{json.RootElement.GetProperty("id").GetString()}");
                }
                catch (Exception ex) when (ex is JsonException || ex is InvalidOperationException)
                {
                    return BadRequest(ex);
                }
                catch (Exception ex)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, ex);
                }
            //}
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetDeivceSchema(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var result = await _deviceRegistryService.GetDeviceSchema(id);

                return (result != null) ? Ok(result) : NotFound(id);
            }
            else
            {
                var error = $"Invalid ID: {id}";
                Console.WriteLine("[{0}] error: {1}", nameof(DeviceRegistryController),
                    error);

                return BadRequest(new { Error = error });
            }
        }
    }
}
