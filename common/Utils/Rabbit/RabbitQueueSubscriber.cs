using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace Utils.Rabbit
{
    public class RabbitQueueSubscriber : IDisposable
    {
        private bool _disposedValue;
        private readonly string _queueName;
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private bool _started;

        public RabbitQueueSubscriber(string hostName, int port, string queueName)
        {
            System.Console.WriteLine("[{0}] info: Connecting to server {1}:{2}, queue {3}",
                nameof(RabbitQueueSubscriber), hostName, port, queueName);

            var factory = new ConnectionFactory { HostName = hostName, Port = port };

            _queueName = queueName;
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.ExchangeDeclare(_queueName, ExchangeType.Fanout);
            _channel.QueueDeclare(queueName, false, false, false);
        }

        public void Start(Action<string> processMessage)
        {
            if (!_started)
            {
                _channel.QueueBind(_queueName, _queueName, "");
                
                var consumer = new EventingBasicConsumer(_channel);
                consumer.Received += (model, eventArgs) =>
                {
                    var body = eventArgs.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);

                    processMessage(message);
                };

                _channel.BasicConsume(_queueName, true, consumer);
                _started = true;
            }
        }

        public void Stop()
        {
            if (_started)
            {
                _channel.BasicCancel(_queueName);
                _started = false;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    if (_started)
                    {
                        Stop();
                    }

                    _channel.Dispose();
                    _connection.Dispose();
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}