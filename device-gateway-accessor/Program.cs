﻿using DeviceGatewayAccessor.Services;
using System;
using System.Threading;
using Utils.Environment;

namespace DeviceGatewayAccessor
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting {0}...", nameof(DeviceGatewayAccessor));
            using (var telemetryService = new DeviceTelemetryService(
                EnvironmentUtils.GetEnvironmentVariable(Constants.RabbitMqBrokerServer),
                int.Parse(EnvironmentUtils.GetEnvironmentVariable(Constants.RabbitMqBrokerPort, "5672")),
                EnvironmentUtils.GetEnvironmentVariable(Constants.DeviceInputQueueName),
                EnvironmentUtils.GetEnvironmentVariable(Constants.DeviceTelemetryQueueName)
            ))
            using (var updateService = new DeviceUpdateService(
                EnvironmentUtils.GetEnvironmentVariable(Constants.RabbitMqBrokerServer),
                int.Parse(EnvironmentUtils.GetEnvironmentVariable(Constants.RabbitMqBrokerPort, "5672")),
                EnvironmentUtils.GetEnvironmentVariable(Constants.DeviceNewStateQueueName),
                EnvironmentUtils.GetEnvironmentVariable(Constants.DeviceUpdateQueueName)
            ))
            {
                telemetryService.Start();
                updateService.Start();
                Thread.Sleep(Timeout.Infinite);
            }
        }
    }
}
