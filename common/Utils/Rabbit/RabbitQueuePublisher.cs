using RabbitMQ.Client;
using System;
using System.Text;

namespace Utils.Rabbit
{
    public class RabbitQueuePublisher : IDisposable
    {
        private bool _disposedValue;
        private readonly string _queueName;
        private readonly IConnection _connection;
        private readonly IModel _channel;

        public RabbitQueuePublisher(string hostName, int port, string queueName)
        {
            System.Console.WriteLine("[{0}] info: Connecting to server {1}:{2}, queue {3}",
                nameof(RabbitQueuePublisher), hostName, port, queueName);

            var factory = new ConnectionFactory { HostName = hostName , Port = port };

            _queueName = queueName;
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.ExchangeDeclare(_queueName, ExchangeType.Fanout);
        }

        public void Send(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);
            _channel.BasicPublish(_queueName, "", null, body);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _connection.Dispose();
                    _channel.Dispose();
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}