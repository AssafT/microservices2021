using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace DeviceEnrollment.Services
{
    public class DeviceEnrollmentService
    {
        private readonly Uri _deviceRegistryAccessorBaseUri;
        private const string IdProperty = "id";

        public DeviceEnrollmentService()
        {
            _deviceRegistryAccessorBaseUri = 
                new Uri($@"http://{Environment.GetEnvironmentVariable(Constants.DeviceRegistryAccessorServer) ?? @"localhost"}:{
                    Environment.GetEnvironmentVariable(Constants.DeviceRegistryAccessorPort) ?? "4242"}/deviceregistry/");
        }

        public async Task Register(string schema)
        {
            var json = JsonDocument.Parse(schema);
            Validate(json);

            using (var httpClient = new HttpClient())
            {
                var httpResponse = await httpClient.PostAsJsonAsync(_deviceRegistryAccessorBaseUri, schema);
                if (httpResponse.Content != null)
                {
                    var responseContent = await httpResponse.Content.ReadAsStringAsync();
                }
                httpResponse.EnsureSuccessStatusCode();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns>null if key doesn't exist.</returns>
        public async Task<string> GetDeviceSchema(string id)
        {
            using (var httpClient = new HttpClient())
            {
                var getByIdUri = new Uri(_deviceRegistryAccessorBaseUri, id.ToString());
                var httpResponse = await httpClient.GetAsync(getByIdUri);
                Console.WriteLine("Path: " + _deviceRegistryAccessorBaseUri);
                httpResponse.EnsureSuccessStatusCode();
                if (httpResponse.Content != null)
                {
                    return await httpResponse.Content.ReadAsStringAsync();
                }
                return null;
            }
        }

        private void Validate(JsonDocument json)
        {
            // TODO: Schema validation; until then:
            GetId(json);
        }

        private string GetId(JsonDocument json)
        {
            return json.RootElement.GetProperty(IdProperty).GetString();
        }
    }


}