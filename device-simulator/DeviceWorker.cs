using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using Utils.Rabbit;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DeviceSimulator
{
    public class DeviceWorker : BackgroundService
    {
        private const string TimeProperty = "time";
        private const string IdProperty = "id";
        private const string StateProperty = "state";

        private readonly ILogger<DeviceWorker> _logger;
        JObject _status;

        public DeviceWorker(ILogger<DeviceWorker> logger)
        {
            _logger = logger;
            _status = new JObject(
                new JProperty(IdProperty, "sim-device-00"),
                new JProperty(StateProperty, "off"));
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            using var devicePublisher = new RabbitQueueWriter("localhost", 5672, "device-input");
            using var deviceUpdateListener = new RabbitQueueSubscriber("localhost", 5672, "device-update");


            deviceUpdateListener.Start(message => {
                UpdateStatusTime();
                devicePublisher.Send(_status.ToString());
            });

            while (!stoppingToken.IsCancellationRequested)
            {
                Console.WriteLine("[{0}] [{1}] info: Sending periodic status", nameof(DeviceSimulator),
                    DateTime.Now.ToString("o"));
                UpdateStatusTime();
                devicePublisher.Send(_status.ToString());
                await Task.Delay(TimeSpan.FromSeconds(5), stoppingToken);
            }            
        }

        private void UpdateStatusTime()
        {
            _status[TimeProperty] = DateTime.Now.ToString("o");
        }
    }
}
