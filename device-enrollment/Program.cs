using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using System;

namespace DeviceEnrollment
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    var serviceUrl = Environment.GetEnvironmentVariable(Constants.DeviceEnrollmentServer) ?? "+";
                    var port = Environment.GetEnvironmentVariable(Constants.DeviceEnrollmentPort) ?? "8082";
                    webBuilder.UseUrls($"http://{serviceUrl}:{port}");
                });
    }
}
