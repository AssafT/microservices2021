import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Transport } from "@nestjs/microservices";

async function bootstrap() {
  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.RMQ,
    options: {
      urls: ['amqp://localhost:5672'],
      queue: 'device-telemetry',
      queueOptions: {
        durable: true
      },
    },
  });
  app.listen(() => {
    console.log("Analysis-dataflow microservice is listening");
  });
}
bootstrap();
