using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;

namespace AnalysisResultAccessor.Services
{

    public class AnalysisResultService
    {
        private IMongoCollection<BsonDocument> _collection;

        public AnalysisResultService()
        {
            Console.WriteLine("Creating mongo client");
            var host = Environment.GetEnvironmentVariable(Constants.MongoDbServer) ?? "localhost";
            var port = Environment.GetEnvironmentVariable(Constants.MongoDbPort) ?? "27017";
            var client = new MongoClient($"mongodb://{host}:{port}");
            var dbName = Environment.GetEnvironmentVariable(Constants.DbName) ?? "microservicesIotExercise";
            var database = client.GetDatabase(dbName);
            _collection = database.GetCollection<BsonDocument>("AnalysisResults");
        }

        public async Task AddAsync(string schema)
        {
            var bson = BsonDocument.Parse(schema);
            await _collection.InsertOneAsync(bson);
        }

        public async Task<List<string>> BrowseAsync(string id)
        {
            var filterBuilder = Builders<BsonDocument>.Filter;
            var filter = filterBuilder.Eq<string>("id", id);

            var results = await _collection.FindAsync(filter);

            var jsonResults = new List<string>();
            await results.ForEachAsync((bson) => jsonResults.Add(JsonConvert.SerializeObject(bson)));
            return jsonResults;
        }
    }


}