namespace DeviceRegistryAccessor
{
    static class Constants
    {
        public const string DeviceRegistryAccessorServer = "DEVICE_REGISTRY_ACCESSOR_SERVER";
        public const string DeviceRegistryAccessorPort = "DEVICE_REGISTRY_ACCESSOR_PORT";
        public const string MongoDbPort = "MONGODB_PORT";
        public const string MongoDbServer = "MONGODB_SERVER";
        public const string DbName = "DB_NAME";
    }
}