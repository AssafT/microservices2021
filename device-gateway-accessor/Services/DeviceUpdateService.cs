using System;
using System.Threading.Tasks;
using Utils.Rabbit;

namespace DeviceGatewayAccessor.Services
{
    class DeviceUpdateService : IDisposable
    {
        private bool _disposedValue;
        private readonly RabbitQueueReader _deviceNewStateReader;
        private readonly RabbitQueuePublisher _deviceUpdatePublisher;
        private readonly Task _stopper;

        public DeviceUpdateService(string queueBrokerHostName, int queueBrokerPort,
            string deviceNewStateQueueName, string deviceUpdaterQueueName)
        {
            _deviceNewStateReader = new RabbitQueueReader(queueBrokerHostName, queueBrokerPort,
                deviceNewStateQueueName);
            _deviceUpdatePublisher = new RabbitQueuePublisher(queueBrokerHostName, queueBrokerPort,
                deviceUpdaterQueueName);
            _stopper = new Task(() => _deviceNewStateReader.Stop());
        }

        public void Start()
        {
            _deviceNewStateReader.Start(message =>
            {
                _deviceUpdatePublisher.Send(message);
                Console.WriteLine("[{0}] debug: {1}", nameof(DeviceUpdateService),
                    "Update message published successfully");

            });
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _stopper.Start();
                    _stopper.Wait();
                    _deviceUpdatePublisher.Dispose();
                    _deviceNewStateReader.Dispose();
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
