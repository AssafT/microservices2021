using Utils.Rabbit;
using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace StateHandlerEngine.Services
{
    class DeviceStateService : IDisposable
    {
        private bool _disposedValue;
        private readonly RabbitQueueSubscriber _telemetrySubscriber;
        private readonly Uri _deviceStateAccessorUrl;
        private readonly Task _stopper;
        private readonly HttpClient _httpClient;

        // queueBrokerName and deviceStateAccessorName are in the form "server:port"
        public DeviceStateService(string queueServerName, int queueServerPort, string telemetryQueueName,
            string deviceStateAccessorName)
        {
            _telemetrySubscriber = new RabbitQueueSubscriber(queueServerName, queueServerPort, telemetryQueueName);
            _stopper = new Task(() => _telemetrySubscriber.Stop());
            _deviceStateAccessorUrl = new Uri($"http://{deviceStateAccessorName}/DeviceState");
            _httpClient = new HttpClient();
        }

        public void Start()
        {
            _telemetrySubscriber.Start(async message =>
            {
                var result = await _httpClient.PostAsJsonAsync(_deviceStateAccessorUrl, message);

                if (result.IsSuccessStatusCode)
                {
                    Console.WriteLine("[{0}] debug: {1}", nameof(DeviceStateService),
                        "message received successfully");
                }
                else
                {
                    Console.WriteLine("[{0}] error: {1} ({2})", nameof(DeviceStateService),
                        result.StatusCode, await result.Content.ReadAsStringAsync());
                }
            });
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _stopper.Start();
                    _stopper.Wait();

                    _httpClient.Dispose();
                    _telemetrySubscriber.Dispose();
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
