#!/bin/bash

echo "Running Docker images..."
gnome-terminal -- docker start -i telemetry-mq
gnome-terminal -- docker start -i device-state-db
# Wait for services to load:
sleep 5

echo "Running DeviceStateAccessor..."
gnome-terminal -- dotnet device-state-accessor/published/DeviceStateAccessor.dll --urls=http://localhost:4266

echo "Running DeviceGatewayAccessor..."
gnome-terminal -- dotnet device-gateway-accessor/published/DeviceGatewayAccessor.dll

echo "Running AnalysisResultAccessor..."
cd analysis-result-accessor
gnome-terminal -- dotnet published/AnalysisResultAccessor.dll --urls=http://localhost:4283
cd -

echo "Running DeviceRegistryAccessor..."
cd device-registry-accessor
gnome-terminal -- dotnet published/DeviceRegistryAccessor.dll --urls=http://localhost:4242
cd -

echo "Running StateHandlerEngine..."
gnome-terminal -- dotnet state-handler-engine/published/StateHandlerEngine.dll --urls=http://localhost:4267

echo "Running DeviceSimulator..."
sleep 5
gnome-terminal -- dotnet device-simulator/published/DeviceSimulator.dll

echo "Running DeviceEnrollemnt..."
cd device-enrollment
gnome-terminal -- dotnet published/DeviceEnrollemnt.dll --urls=http://localhost:8082
cd -

# Insert next project here

echo
echo "Finished loading all services."
