using AnalysisResultAccessor.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace AnalysisResultAccessor.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AnalysisResultController : Controller
    {
        private readonly AnalysisResultService _analysisResultService;

        public AnalysisResultController(AnalysisResultService analysisResultService)
        {
            _analysisResultService = analysisResultService;
        }

        [HttpPost]
        public async Task<IActionResult> Add([FromBody] object body)
        {
            var bodyAsString = body.ToString();

            try
            {
                // just as json simple validation
                var json = JsonDocument.Parse(bodyAsString);

                await _analysisResultService.AddAsync(bodyAsString);
                return Accepted();
            }
            catch (Exception ex) when (ex is JsonException || ex is InvalidOperationException)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> BrowseAsync(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var result = await _analysisResultService.BrowseAsync(id);
                return (result != null) ? Json(result) : NotFound(id);
            }
            else
            {
                var error = $"Invalid ID: {id}";
                Console.WriteLine("[{0}] error: {1}", nameof(AnalysisResultController),
                    error);

                return BadRequest(new { Error = error });
            }
        }
    }
}
