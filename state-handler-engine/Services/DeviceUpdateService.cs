using Utils.Rabbit;
using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace StateHandlerEngine.Services
{
    class DeviceUpdateService : IDisposable
    {
        private bool _disposedValue;
        private readonly RabbitQueueWriter _updateWriter;
        private readonly Uri _deviceStateAccessorUrl;
        private readonly Uri _deviceRegistryAccessorUrl;
        private readonly HttpClient _deviceStateHttpClient;
        private readonly HttpClient _deviceRegistryHttpClient;

        public DeviceUpdateService(string queueServerName, int queueServerPort, string deviceNewStateQueueName,
            string deviceStateAccessorName, string deviceRegistryAccessorName)
        {
            _updateWriter = new RabbitQueueWriter(queueServerName, queueServerPort, deviceNewStateQueueName);
            _deviceStateAccessorUrl = new Uri($"http://{deviceStateAccessorName}/DeviceState");
            _deviceRegistryAccessorUrl = new Uri($"http://{deviceRegistryAccessorName}/DeviceRegistry");
            _deviceStateHttpClient = new HttpClient();
            _deviceRegistryHttpClient = new HttpClient();
        }

        public bool Update(string message)
        {
            try
            {
                var t1 = PostMessageAsync(_deviceStateHttpClient, _deviceStateAccessorUrl, message);
                var t2 = PostMessageAsync(_deviceRegistryHttpClient, _deviceRegistryAccessorUrl, message);
                var t3 = Task.Run(() => _updateWriter.Send(message));

                Task.WaitAll(t1, t2, t3);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("[{0}] error: {1}", nameof(DeviceStateService), ex);

                return false;
            }
        }

        private async Task PostMessageAsync(HttpClient client, Uri url, string message)
        {
                var result = await client.PostAsJsonAsync(url, message);

                if (result.IsSuccessStatusCode)
                {
                    Console.WriteLine("[{0}] debug: {1} {2}", nameof(DeviceStateService),
                        "message sent successfully to", url);
                }
                else
                {
                    Console.WriteLine("[{0}] error: {1} - {2}", nameof(DeviceStateService),
                        url, await result.Content.ReadAsStringAsync());
                }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _deviceStateHttpClient.Dispose();
                    _deviceRegistryHttpClient.Dispose();
                    _updateWriter.Dispose();
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
