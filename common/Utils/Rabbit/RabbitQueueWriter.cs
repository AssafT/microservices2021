using RabbitMQ.Client;
using System;
using System.Text;

namespace Utils.Rabbit
{
    public class RabbitQueueWriter : IDisposable
    {
        private bool _disposedValue;
        private readonly string _queueName;
        private readonly IConnection _connection;
        private readonly IModel _channel;

        public RabbitQueueWriter(string hostName, int port, string queueName)
        {
            System.Console.WriteLine("[{0}] info: Connecting to server {1}:{2}, queue {3}",
                nameof(RabbitQueueWriter), hostName, port, queueName);

            var factory = new ConnectionFactory { HostName = hostName , Port = port };

            _queueName = queueName;
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(_queueName, false, false);
        }

        public void Send(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);
            _channel.BasicPublish("", _queueName, null, body);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _channel.Dispose();
                    _connection.Dispose();
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}