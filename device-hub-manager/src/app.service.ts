import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/common';


@Injectable()
export class AppService {
    constructor(private httpService: HttpService) {}

    setDeviceState(deviceId: string, data) {
        return this.httpService.post(`http://localhost:4267/StateHandler/${deviceId}`, data);
    }
}
