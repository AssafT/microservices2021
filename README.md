# Microservices Course 2021

## Setup & configurations

### VSCode

Install [VSCode for Linux](https://code.visualstudio.com/docs/setup/linux)

### C#:

1. Install the C# extension for VSCode
2. Install [.NET Core SDK](https://docs.microsoft.com/en-us/dotnet/core/install/linux-ubuntu#1804-)

### NestJS

### Docker

### Docker Compose

Update to version 1.28.2:

```
sudo apt-get remove docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.28.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

### Containers (before exercise 3)

Run the following commands once to create Docker containers:

#### MongoDB

```
docker create -p 27018:27017 --name x-db -v x-db-fs:/data/db -v x-db-config-fs:/data/configdb mongo:4.4.3-bionic
```

#### RabbitMQ

```
docker create -p 5672:5672 -p 15672:15672 --name telemetry-mq -v telemetry-mq-fs:/var/lib/rabbitmq rabbitmq:3.8.11-management-alpine
```

#### Redis

```
docker create -p 6379:6379 --name device-state-db -v device-state-db-fs:/data redis:6.0.10-alpine
```

## Building and running

To build all services, execute the following command from the root directory of this repo:
```
./build.sh
```

To run all services, execute the following command from the root directory of this repo:
```
./run.sh
```

To build all services and run them immediately, execute the following command from the root directory of this repo:
```
./build_and_run.sh
```
