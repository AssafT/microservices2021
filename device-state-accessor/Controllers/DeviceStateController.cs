using DeviceStateAccessor.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace DeviceStateAccessor.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DeviceStateController : ControllerBase
    {
        private readonly DeviceStateService _deviceStateService;
        private readonly ILogger<DeviceStateController> _logger;

        public DeviceStateController(DeviceStateService deviceStateService, ILogger<DeviceStateController> logger)
        {
            _deviceStateService = deviceStateService;
            _logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> Update()
        {
            using (var reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                var body = await reader.ReadToEndAsync();

                try
                {
                    var json = JsonDocument.Parse(body);

                    return _deviceStateService.Set(body)
                        ? Ok() : StatusCode(StatusCodes.Status500InternalServerError);
                }
                catch (Exception ex) when (ex is JsonException || ex is InvalidOperationException)
                {
                    return BadRequest(ex);
                }
                catch (Exception ex)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, ex);
                }
            }
        }

        [HttpGet("{id}")]
        public IActionResult Get(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var result = _deviceStateService.Get(id);

                return (result != null) ? Ok() : NotFound(id);
            }
            else
            {
                var error = $"Invalid ID: {id}";
                Console.WriteLine("[{0}] error: {1}", nameof(DeviceStateController),
                    error);

                return BadRequest(new { Error = error });
            }
        }
    }
}
