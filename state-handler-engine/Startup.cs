using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using StateHandlerEngine.Services;
using Utils.Environment;

namespace StateHandlerEngine
{
    public class Startup
    {
        private const string Version = "v1";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var deviceStateAccessorName
                = $@"{EnvironmentUtils.GetEnvironmentVariable(Constants.DeviceStateAccessorServer)}:{
                    EnvironmentUtils.GetEnvironmentVariable(Constants.DeviceStateAccessorPort)}";

            var deviceRegistryAccessorName
                = $@"{EnvironmentUtils.GetEnvironmentVariable(Constants.DeviceRegistryAccessorServer)}:{
                    EnvironmentUtils.GetEnvironmentVariable(Constants.DeviceRegistryAccessorPort)}";

            services.AddScoped<DeviceUpdateService>(_ => new DeviceUpdateService(
                EnvironmentUtils.GetEnvironmentVariable(Constants.RabbitMqBrokerServer),
                int.Parse(EnvironmentUtils.GetEnvironmentVariable(Constants.RabbitMqBrokerPort, "5672")),
                EnvironmentUtils.GetEnvironmentVariable(Constants.DeviceNewStateQueueName),
                deviceStateAccessorName, deviceRegistryAccessorName)
            );

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(Version, new OpenApiInfo { Title = nameof(StateHandlerEngine), Version = Version });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint($"/swagger/{Version}/swagger.json",
                    $"nameof(StateHandlerEngine) {Version}"));
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
