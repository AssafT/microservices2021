#!/bin/bash


echo
echo "Building DeviceRegistryAccessor..."
cd device-registry-accessor
dotnet restore
dotnet publish -c Release -o published
cd -
echo "Finished building DeviceRegistryAccessor."


echo
echo "Building DeviceEnrollemnt..."
cd device-enrollment
dotnet restore
dotnet publish -c Release -o published
cd -
echo "Finished building DeviceEnrollemnt."


echo
echo "Building AnalysisResultAccessor..."
cd analysis-result-accessor
dotnet restore
dotnet publish -c Release -o published
cd -
echo "Finished building AnalysisResultAccessor."


echo
echo "Building DeviceStateAccessor..."
cd device-state-accessor
dotnet clean -o published -v m
dotnet restore
dotnet publish -c Release -o published
cd -
echo "Finished building DeviceStateAccessor."

echo
echo "Building DeviceGatewayAccessor..."
cd device-gateway-accessor
dotnet clean -o published -v m
dotnet restore
dotnet publish -c Release -o published
cd -
echo "Finished building DeviceGatewayAccessor."

echo
echo "Building StateHandlerEngine..."
cd state-handler-engine
dotnet clean -o published -v m
dotnet restore
dotnet publish -c Release -o published
cd -
echo "Finished building StateHandlerEngine."

echo
echo "Building DeviceSimulator..."
cd device-simulator
dotnet clean -o published -v m
dotnet restore
dotnet publish -c Release -o published
cd -
echo "Finished building DeviceSimulator."

# Insert next project here

echo
echo "Finished building all services."
