namespace DeviceEnrollment
{
    static class Constants
    {
        public const string DeviceRegistryAccessorServer = "DEVICE_REGISTRY_ACCESSOR_SERVER";
        public const string DeviceRegistryAccessorPort = "DEVICE_REGISTRY_ACCESSOR_PORT";
        public const string DeviceEnrollmentServer = "DEVICE_ENROLLMENT_SERVER";
        public const string DeviceEnrollmentPort = "DEVICE_ENROLLMENT_PORT";
    }
}