﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StateHandlerEngine.Services;
using System;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace StateHandlerEngine.Controllers
{
    [ApiController]
    [Route("[controller]")]
    class StateHandlerController : ControllerBase
    {
        private readonly DeviceUpdateService _deviceUpdateService;
        private readonly ILogger<StateHandlerController> _logger;

        public StateHandlerController(DeviceUpdateService deviceUpdateService, ILogger<StateHandlerController> logger)
        {
            _deviceUpdateService = deviceUpdateService;
            _logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> Update()
        {
            using (var reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                var body = await reader.ReadToEndAsync();

                try
                {
                    var json = JsonDocument.Parse(body);

                    return _deviceUpdateService.Update(body)
                        ? Ok() : StatusCode(StatusCodes.Status500InternalServerError);
                }
                catch (Exception ex) when (ex is JsonException || ex is InvalidOperationException)
                {
                    return BadRequest(ex);
                }
                catch (Exception ex)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, ex);
                }
            }
        }
    }
}
