#!/bin/bash

echo
echo "Building DeviceHubManager..."
cd device-hub-manager && npm run build
npm run start
cd ..
echo "Finished building DeviceHubManager."

echo
echo "Building RawTelemetryEngine..."
cd raw-telemetry-accessor && npm run build
npm run start
cd -
echo "Finished building DeviceGatewayAccessor."

echo
echo "Building AnalysisDataflowEngine..."
cd analysis-dataflow-engine && npm run build
npm run start
cd -
echo "Finished building AnalysisDataflowEngine."

echo
echo "Finished building all services."
