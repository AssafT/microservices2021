using DeviceEnrollment.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace DeviceEnrollment.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DeviceEnrollmentController : Controller
    {

        private readonly DeviceEnrollmentService _deviceEnrollmentService;

        public DeviceEnrollmentController(DeviceEnrollmentService deviceEnrollmentService)
        {
            _deviceEnrollmentService = deviceEnrollmentService;
        }

        [HttpPost("")]
        public async Task<IActionResult> Register([FromBody] object body)
        {
            var bodyAsString = body.ToString();

            try
            {
                await _deviceEnrollmentService.Register(bodyAsString);
                var json = JsonDocument.Parse(bodyAsString);
                return Accepted($"DeviceEnrollment/{json.RootElement.GetProperty("id").GetString()}");
            }
            catch (Exception ex) when (ex is JsonException || ex is InvalidOperationException)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetDeviceSchema(string id)
        {
            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    var result = await _deviceEnrollmentService.GetDeviceSchema(id);

                    return (result != null) ? Ok(result) : NotFound(id);
                }
                else
                {
                    var error = $"Invalid ID: {id}";
                    Console.WriteLine("[{0}] error: {1}", nameof(DeviceEnrollmentController),
                        error);

                    return BadRequest(new { Error = error });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }
    }
}
