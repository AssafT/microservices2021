using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace DeviceSimulator
{
    public class Program
    {
        public static void Main(string[] args)
        {
            System.Console.WriteLine("Starting {0}...", nameof(DeviceSimulator));
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<DeviceWorker>();
                });
    }
}
