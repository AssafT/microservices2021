using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using System;

namespace AnalysisResultAccessor
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    var serviceUrl = Environment.GetEnvironmentVariable(Constants.AnalysisResultAccessorServer) ?? "+";
                    var port = Environment.GetEnvironmentVariable(Constants.AnalysisResultAccessorPort) ?? "4283";
                    webBuilder.UseUrls($"http://{serviceUrl}:{port}");
                });
    }
}
