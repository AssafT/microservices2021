namespace StateHandlerEngine
{
    static class Constants
    {
        public const string StateHandlerEnginePort = "STATE_HANDLER_ENGINE_PORT";
        public const string RabbitMqBrokerServer = "RABBITMQ_AGENT_SERVER";
        public const string RabbitMqBrokerPort = "RABBITMQ_AGENT_PORT";
        public const string DeviceTelemetryQueueName = "DEVICE_TELEMETRY_QUEUE_NAME";
        public const string DeviceNewStateQueueName = "DEVICE_NEW_STATE_QUEUE_NAME";
        public const string DeviceStateAccessorServer = "DEVICE_STATE_ACCESSOR_SERVER";
        public const string DeviceStateAccessorPort = "DEVICE_STATE_ACCESSOR_PORT";
        public const string DeviceRegistryAccessorServer = "DEVICE_REGISTRY_ACCESSOR_SERVER";
        public const string DeviceRegistryAccessorPort = "DEVICE_REGISTRY_ACCESSOR_PORT";
    }
}