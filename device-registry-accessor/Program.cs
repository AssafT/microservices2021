using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using System;

namespace DeviceRegistryAccessor
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    var serviceUrl = Environment.GetEnvironmentVariable(Constants.DeviceRegistryAccessorServer) ?? "+";
                    var port = Environment.GetEnvironmentVariable(Constants.DeviceRegistryAccessorPort) ?? "4242";
                    webBuilder.UseUrls($"http://{serviceUrl}:{port}");
                });
    }
}
