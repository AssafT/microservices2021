﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using StateHandlerEngine.Services;
using System;
using Utils.Environment;
namespace StateHandlerEngine
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Starting {0}...", nameof(StateHandlerEngine));
            
            var deviceStateAccessorName
                = $@"{EnvironmentUtils.GetEnvironmentVariable(Constants.DeviceStateAccessorServer)}:{
                    EnvironmentUtils.GetEnvironmentVariable(Constants.DeviceStateAccessorPort)}";

            using var service = new DeviceStateService(
                EnvironmentUtils.GetEnvironmentVariable(Constants.RabbitMqBrokerServer),
                int.Parse(EnvironmentUtils.GetEnvironmentVariable(Constants.RabbitMqBrokerPort, "5672")),
                EnvironmentUtils.GetEnvironmentVariable(Constants.DeviceTelemetryQueueName),
                deviceStateAccessorName);
            service.Start();
            CreateHostBuilder(args).Build().Run();
		}

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseUrls(
                        $"http://localhost:{EnvironmentUtils.GetEnvironmentVariable(Constants.StateHandlerEnginePort)}");
                });
    }
}
