namespace DeviceGatewayAccessor
{
    static class Constants
    {
        public const string RabbitMqBrokerServer = "RABBITMQ_AGENT_SERVER";
        public const string RabbitMqBrokerPort = "RABBITMQ_AGENT_PORT";
        public const string DeviceTelemetryQueueName = "DEVICE_TELEMETRY_QUEUE_NAME";
        public const string DeviceNewStateQueueName = "DEVICE_NEW_STATE_QUEUE_NAME";
        public const string DeviceInputQueueName = "DEVICE_INPUT_QUEUE_NAME";
        public const string DeviceUpdateQueueName = "DEVICE_UPDATE_QUEUE_NAME";
        public const string DeviceRegistryAccessorServer = "DEVICE_REGISTRY_ACCESSOR_SERVER";
        public const string DeviceRegistryAccessorPort = "DEVICE_REGISTRY_ACCESSOR_PORT";
    }
}