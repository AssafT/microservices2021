namespace AnalysisResultAccessor
{
    static class Constants
    {
        public const string AnalysisResultAccessorServer = "ANALYSIS_RESULT_ACCESSOR_SERVER";
        public const string AnalysisResultAccessorPort = "ANALYSIS_RESULT_ACCESSOR_PORT";
        public const string MongoDbPort = "MONGODB_PORT";
        public const string MongoDbServer = "MONGODB_SERVER";
        public const string DbName = "DB_NAME";
    }
}