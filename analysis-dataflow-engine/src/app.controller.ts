import { Controller } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { AppService } from './app.service';


@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @EventPattern('deviceTelemetry')
  async getTelemetry(data) {
    const deviceTelemetry = this.appService.getDeviceTelemetry(data.id);
    // Business logic
    return this.appService.setAnalysisResult(data.id, deviceTelemetry);
  }
}
