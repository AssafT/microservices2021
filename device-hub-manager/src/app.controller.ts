import { Controller, Post, Param, Body } from '@nestjs/common';
import { AppService } from './app.service';

@Controller('/api/deviceState')
export class AppController {
  constructor(private readonly appService: AppService) {}
    
  @Post('/:id')
  updateState(@Param('id') id: string, @Body() deviceState) {
    this.appService.setDeviceState(id, { state: deviceState });
  }
}
