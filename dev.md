# Microservices Course 2021 - developer notes

## Managers

### Device Enrollment (Marik)

Listens to HTTP POST (registers schema) on `http://localhost:8082/DeviceEnrollment/`.
Listens to HTTP GET (fetch device schema by id) on `http://localhost:8082/DeviceEnrollment/{id}`.

## Engines

### State Handler Engine (Assaf)

Device telemetry reports:
* Subscribes to `device-telemetry` queue
* Writes reported device state to Device State Accessor

Device state change requests:
* Listens to HTTP POST on `http://localhost:4267/StateHandler`
* Writes requested device state to `device-new-state` queue on `localhost` with default port

## Accessors

### Raw Telemetry Accessor (Ron)

Listens to HTTP GET on `http://localhost:3001/deviceTelemetry/{deviceId}`.
<!-- Listens to HTTP POST on `http://localhost:3001/deviceTelemetry/{deviceId}`. -->
Writes to MongoDB.

### Device Gateway Accessor (Assaf)

Device telemetry reports:
* Listens to `device-input` queue on `localhost` with default port
* Publishes to `device-telemetry` queue on `localhost` with default port

Device state change requests:
* Listens to `device-new-state` queue on `localhost` with default port
* Publishes to `device-update` queue on `localhost` with default port

### Device State Accessor (Assaf)

Listens to HTTP POST on `http://localhost:4266/DeviceState`.
Listens to HTTP GET on `http://localhost:4266/DeviceState/{id}`.
Writes to Redis.


### Device Registry Accessor (Marik)

Listens to HTTP POST on `http://localhost:4242/DeviceRegistry`.
Listens to HTTP GET on `http://localhost:4242/DeviceRegistry/{id}`.
Using MongoDB

### Analysis Result Accessor (Marik)

Listens to HTTP POST on `http://localhost:4283/AnalysisResult`.
Listens to HTTP GET (Browse all records of id) on `http://localhost:4283/AnalysisResult/{id}`.
Using MongoDB

## Simulators

### Web client

### Device Simulator (Assaf)

Writes to `device-input` queue on `localhost` with default port.
Subscribes to `device-update` queue on `localhost` with default port

## Schemes

### Device telemetry report

```
{
    "id" : "df67ce99-f1b9-432e-8cba-0eb592a99a19",
    "timestamp" : "2021-01-28T20:03:00Z",
    "state" : "off"
    ...
}
```

### Device update request

```
{
    "id" : "df67ce99-f1b9-432e-8cba-0eb592a99a19",
    "timestamp" : "2021-01-28T20:05:30Z",
    "state" : {
        "mode" : "heat",
        "temp" : 25,
        "fan" : "auto"
    }
    ...
}
```
