using System;
using System.Threading.Tasks;
using Utils.Rabbit;

namespace DeviceGatewayAccessor.Services
{
    class DeviceTelemetryService : IDisposable
    {
        private bool _disposedValue;
        private readonly RabbitQueueReader _deviceReader;
        private readonly RabbitQueuePublisher _telemetryPublisher;
        private readonly Task _stopper;

        public DeviceTelemetryService(string queueBrokerHostName, int queueBrokerPort, string deviceReaderQueueName,
            string telemetryPublisherQueueName)
        {
            _deviceReader = new RabbitQueueReader(queueBrokerHostName, queueBrokerPort, deviceReaderQueueName);
            _telemetryPublisher
                = new RabbitQueuePublisher(queueBrokerHostName, queueBrokerPort, telemetryPublisherQueueName);
            _stopper = new Task(() => _deviceReader.Stop());
        }

        public void Start()
        {
            _deviceReader.Start(message =>
            {
                _telemetryPublisher.Send(message);
                Console.WriteLine("[{0}] debug: {1}", nameof(DeviceTelemetryService),
                    "message published successfully");

            });
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _stopper.Start();
                    _stopper.Wait();
                    _telemetryPublisher.Dispose();
                    _deviceReader.Dispose();
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
