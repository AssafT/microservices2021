using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Utils.Environment;

namespace DeviceStateAccessor
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Starting {0}...", nameof(DeviceStateAccessor));
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseUrls(
                        $"http://localhost:{EnvironmentUtils.GetEnvironmentVariable(Constants.DeviceStateAccessorPort)}");
                });
    }
}
