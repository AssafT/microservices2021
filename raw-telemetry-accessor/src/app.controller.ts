import { Controller, Get, Param, Post } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/:id')
  getRawTelemetry(@Param('id') id: string): string {
    return this.appService.getHello();
  }
}
